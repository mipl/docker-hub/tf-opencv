ARG BASE_IMAGE
FROM ${BASE_IMAGE}
LABEL author="mipl"


RUN apt-get -y update -qq && \
    apt-get -y install \
                       # Required
                       build-essential \
                       cmake \
                       nano \
                       git \
                       pkg-config \
                       vim \
    && \
    pip install \
        dlib \
        h5py \
        jupyterlab \
        mnist \
        slackclient \
        tqdm \
        urllib3 \
    &&\
    \
    apt-get autoclean autoremove &&\
    rm -rf /var/lib/apt/lists/* \
           /tmp/* \
           /var/tmp/*